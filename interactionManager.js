import camera from './camera';
import renderer from './renderer';
import { InteractionManager } from 'three.interactive';

const interactionManager = new InteractionManager(
  renderer,
  camera,
  renderer.domElement
);

export default interactionManager;
