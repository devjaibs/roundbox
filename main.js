import './style.css';
import * as THREE from 'three'; 
import {
	BoxGeometry,
	BufferGeometry,
	CircleGeometry,
	Color,
	ConeGeometry,
	Curve,
	CylinderGeometry,
	DodecahedronGeometry,
	DoubleSide,
	ExtrudeGeometry,
	Float32BufferAttribute,
	Group,
	IcosahedronGeometry,
	LatheGeometry,
	LineSegments,
	LineBasicMaterial,
	Mesh,
	MeshPhongMaterial,
	OctahedronGeometry,
	PerspectiveCamera,
	PlaneGeometry,
	PointLight,
	RingGeometry,
	Scene,
	Shape,
	ShapeGeometry,
	SphereGeometry,
	TetrahedronGeometry,
	TorusGeometry,
	TorusKnotGeometry,
	TubeGeometry,
	Vector2,
	Vector3,
	WireframeGeometry,
	WebGLRenderer
} from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { RGBELoader } from  'three/examples/jsm/loaders/RGBELoader.js';
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader.js';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import { RoundedBoxGeometry } from 'three/examples/jsm/geometries/RoundedBoxGeometry';
import { InteractionManager } from 'three.interactive';

let text,group;
const scene = new Scene();


const camera = new PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 50 );
camera.position.z = 6;

const renderer = new WebGLRenderer( { antialias: true, canvas: document.querySelector('canvas'),alpha: true } );
renderer.setPixelRatio( window.devicePixelRatio / 1.2);
renderer.setSize( window.innerWidth, window.innerHeight );

const pmremGenerator = new THREE.PMREMGenerator( renderer );
new RGBELoader().load( 'environment.hdr', function ( texture ) {

	texture.mapping = THREE.EquirectangularReflectionMapping;

	scene.environment = texture;
    scene.background = texture;
	console.log(scene);
});

const interactionManager = new InteractionManager(
	renderer,
	camera,
	renderer.domElement
  );

const orbit = new OrbitControls( camera, renderer.domElement );

const yourtext = 'Click!';

const boxwidth = yourtext.length / 1.5;

const geometry = new RoundedBoxGeometry( boxwidth, 1.8, 0.8 );
const material = new THREE.MeshStandardMaterial( {color: 'pink',roughness: 0.5} );
const cube = new THREE.Mesh( geometry, material );
scene.add( cube );
interactionManager.add(cube);


	const loader = new FontLoader();

loader.load( 'font-regular.json', function ( font ) {

	   const textGeometry = new TextGeometry( yourtext, {
		font: font,
		size: 0.6,
		height: 0.3,
		curveSegments: 12,
		bevelEnabled: true,
		bevelThickness: 0.1,
		bevelSize: 0.05,
		bevelOffset: 0,
		bevelSegments: 10
	} );
	const textmaterial = new THREE.MeshStandardMaterial({
		 color: 'white',
         roughness: 0.3
	})

     text = new THREE.Mesh(textGeometry,textmaterial);
	scene.add(text);
	text.position.set(0,0,0.3);
	text.translateX(-yourtext.length * 0.15);
	text.translateY(-0.3);

 group = new THREE.Group();
group.add( cube );
group.add( text );

scene.add( group );

} );

function update(){

	
	
	cube.addEventListener('mouseover', (event) =>{
	
		document.body.style.cursor = 'pointer';
   });

   cube.addEventListener('mouseout', (event) =>{
	
	document.body.style.cursor = 'default';
});


cube.addEventListener('click', (event) =>{
	
group.scale.set( 1.0, 1.0, 1.0);
});

cube.addEventListener('mousedown', (event) =>{
	
	group.scale.set(0.9, 0.9, 0.9);
	});
		

	};



function render() {

	requestAnimationFrame( render );
update();
	renderer.render( scene, camera );

}



window.addEventListener( 'resize', function () {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );

}, false );

render();